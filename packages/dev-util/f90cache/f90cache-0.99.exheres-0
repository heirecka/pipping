# Copyright 2011 Elias Pipping <pipping@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Fortran compiler cache"
DESCRIPTION="It acts as a caching pre-processor to Fortran compilers, using the -E compiler switch
and a hash to detect when a compilation can be satisfied from cache. This often results in a great
speedup in common compilations.
"
HOMEPAGE="https://perso.univ-rennes1.fr/edouard.canot/${PN}"
DOWNLOADS="${HOMEPAGE}/${PNV}.tar.bz2"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES=""

src_prepare() {
    edo sed -e '/strip /d' -i Makefile.in
}

src_install() {
    default

    dodir /usr/$(exhost --target)/libexec/f90cache
    insinto /usr/$(exhost --target)/libexec/f90cache
    local v
    for v in 7 8; do
        # FIXME: f90cache requires the name to be of the form gfortran-4.x
        dosym /usr/$(exhost --target)/bin/f90cache /usr/$(exhost --target)/libexec/f90cache/gfortran-${v}
    done

    keepdir /var/cache/f90cache
}

pkg_postinst() {
    edo chown paludisbuild:paludisbuild /var/cache/f90cache

    einfo "In order to use ${PN} in conjunction with paludis, please add"
    einfo "something like the following to your /etc/paludis/bashrc:"
    einfo ""
    einfo '# f90cache'
    einfo "PATH=\"/usr/$(exhost --target)/libexec/f90cache:\${PATH}\""
    einfo 'F90CACHE_DIR="/var/cache/f90cache"'
    einfo 'F77=gfortran-8'
    einfo 'FORTRAN=${F77} # Overriding profile!'
}

